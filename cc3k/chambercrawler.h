#ifndef __CC_H__
#define __CC_H__
#include <sstream>
#include <iostream>
#include "grid.h"

class Hero;

class ChamberCrawler {
   bool won, lost;
   //keeps track if merchants are aggressive, true = aggressive
   bool merchantFlag;
   //new game flag
   bool newgame;
   //The pointer to Grid object of the game
   Grid *map;
   //The Level
   int level;
   //Are you at stairs
   bool isNextFloor;
   int dir;
   bool dflt;
   //The reference to Hero object of the game
   Hero * pc;
   //PC's action, use for display
   std::stringstream prevAct;
   std::string *lvl; //Stores the string for the map of the levels.
   public:
      ChamberCrawler();
      ~ChamberCrawler();
      //gets merchant aggression
      bool aggressiveMerchant();
      //sets merchants aggression
      void setAggressiveMerchant();

      void changeDflt();
      //Start new game, pass the race that user picked as arg
      void newGame(char race);
      //Generates list of maps based on a string passed by user
      void setLevels(std::string map);
      //gets the command from user
      void takeCmd(std::string s);
      //initializes the fields
      void init();
      //clears the current game
      void clearGame();
      //update game
      void update();
      bool validDir(std::string ss,std::string cmd);
      void print();

      void setLastAction(std::stringstream & ss);
      void clearLastAction();

      //returns whether its a new game 
      bool isNewGame();
      bool isLost();
      bool isWon();
};
#endif
