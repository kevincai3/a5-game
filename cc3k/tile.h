#ifndef __TILE_H__
#define __TILE_H__
#include <string>
#include <vector>

class Sprite;
class Items;

class Tile{
   protected:
      std::vector<Tile*> neighbours;//neighbour tiles
      Sprite * pc;//
      Items * item;
   public:
      Tile();
      virtual ~Tile();

      //gets the proper character to be displayed
      virtual char display() = 0;
      virtual void setStairs();
      virtual bool getStairs();
      //see if tile is enterable
      virtual bool isEnterableHero();
      virtual bool isEnterableEnemy();
      //see whether tile contains something
      bool heroIsTaken();
      bool enemyIsTaken();

      //sets a sprite
      void setSprite(Sprite * s);

      //gets a sprite
      Sprite *getSprite();

      //removes the sprite
      void removeSprite();

      //sets a item
      void setItem(Items * i);

      //gets a item
      Items * getItem();

      //removes the item
      void removeItem();

      //returns the corresponding direction given placement
      std::string mapDirection(int i);

      void setNeighbour(Tile *t);
      int numOfNeighbours();
      Tile *getNeighbour(int i);
};

#endif
