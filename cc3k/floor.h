#ifndef __FLOOR_H__
#define __FLOOR_H__
#include "tile.h"

class Floor : public Tile{
   bool hasStairs;
   public:
      Floor();
      ~Floor();
      char display();
      void setStairs();
      bool getStairs();
      bool isEnterableHero();
      bool isEnterableEnemy();
};
#endif
