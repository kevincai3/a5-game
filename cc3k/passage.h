#ifndef __PASS_H__
#define __PASS_H__
#include "tile.h"

class Passage : public Tile{
   public:
      Passage();
      ~Passage();
      char display();
      bool isEnterableHero();
      bool isEnterableEnemy();
};
#endif
