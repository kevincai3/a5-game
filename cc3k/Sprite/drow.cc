#include <string>
#include <sstream>
#include "drow.h"
#include "../chambercrawler.h"

using namespace std;

Drow::Drow(){
   hp = maxHp = 150;
   atk = currentAtk = 25;
   def = currentDef = 25;
}

string Drow::name(){
   return "Drow";
}

void Drow::usePotion(string name, double hp, double atk, double def){
   stringstream ss("");
   hp *= 1.5;
   atk *= 1.5;
   def *= 1.5;
   heal(hp);
   currentAtk += atk;
   currentDef += def;
   ss << this->name() << " used " << name << ".";
   cc->setLastAction(ss);
   usedPots.push_back(name);
}
