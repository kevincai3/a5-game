#ifndef __ENEMY_H__
#define __ENEMY_H__
#include <string>
#include "sprite.h"

class Enemy : public Sprite {
   protected:
      virtual int generateGold();
   public:
      virtual void onDeath(Sprite &s);
      virtual char display() = 0;
};
#endif
