#ifndef __SHADE_H__
#define __SHADE_H__
#include <string>
#include "hero.h"

class Shade : public Hero {
   public:
      Shade();
      std::string name();
      double getScore();
};
#endif
