#include <string>
#include "dwarf.h"

using namespace std;

Dwarf::Dwarf(){
   hp = 100;
   atk = 20;
   def = 30;
}

string Dwarf::name(){
   return "Dwarf";
}

char Dwarf::display(){
   return 'W';
}
