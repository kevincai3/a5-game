#ifndef __HUMAN_H__
#define __HUMAN_H__
#include <string>
#include "enemy.h"

using namespace std;

class Human : public Enemy {
   public:
      Human();
      string name();
      void onDeath(Sprite &s);
      char display();
};
#endif
