#ifndef __HALFLING_H__
#define __HALFLING_H__
#include <string>
#include <stdlib.h>
#include "enemy.h"

class Halfling : public Enemy {
   public:
      Halfling();
      std::string name();
      int defend(Sprite &s, int dmg);
      char display();
};
#endif
