#ifndef __TROLL_H__
#define __TROLL_H__

#include <string>
#include "hero.h"

class Troll : public Hero {
   public:
      Troll();
      std::string name();
      void endTurn();
};
#endif
