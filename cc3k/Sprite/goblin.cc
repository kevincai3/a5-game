#include <sstream>
#include <string>
#include "goblin.h"
#include "../chambercrawler.h"

using namespace std;

Goblin::Goblin(){
   hp = maxHp = 110;
   atk = currentAtk = 15;
   def = currentDef = 20;
};

string Goblin::name(){
   return "Goblin";
}

void Goblin::onKill(){
   stringstream ss("");
   gold += 5;
   ss << name() << " has looted 5 gold " << gold << ".";
   cc->setLastAction(ss);
}
