#ifndef __DWARF_H__
#define __DWARF_H__
#include <string>
#include "enemy.h"

class Dwarf : public Enemy {
   public:
      Dwarf();
      std::string name();
      char display();
};

#endif
