#ifndef __VAMPIRE_H__
#define __VAMPIRE_H__
#include <string>
#include "hero.h"

class Vampire : public Hero {
   public:
      Vampire();
      std::string name();
      void attack(Sprite &s);
};

#endif
