#include <sstream>
#include <string>
#include "human.h"
#include "sprite.h"
#include "../chambercrawler.h"
#include "../tile.h"

using namespace std;

Human::Human(){
   hp = 140;
   atk = def = 20;
}

string Human::name(){
   return "Human";
}

void Human::onDeath(Sprite &s){
   stringstream ss("");
   s.gainGold('K', generateGold());
   s.gainGold('K', generateGold());
   ss << s.name() << " has slain " << name() << " and recieved " << gold << " gold.";
   cc->setLastAction(ss);
   s.onKill();
   pos->removeSprite();
}

char Human::display() {
   return 'H';
}
