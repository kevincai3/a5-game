#include <sstream>
#include <cstdlib>
#include <string.h>
#include "enemy.h"
#include "../chambercrawler.h"
#include "../tile.h"

using namespace std;

int Enemy::generateGold(){
   return rand() % 2 + 1;
}

void Enemy::onDeath(Sprite &s){
   stringstream ss("");
   int gold = generateGold();
   s.gainGold('K', gold);
   ss << s.name() << " has slain " << name() << ".";
   cc->setLastAction(ss);
   s.onKill();
   pos->removeSprite();
}
