#include <sstream>
#include <string>
#include <cstdlib>
#include "halfling.h"
#include "../chambercrawler.h"

using namespace std;

Halfling::Halfling(){
   hp = 100;
   atk = 15;
   def = 20;
}

int Halfling::defend(Sprite &s, int dmg){
   int hit = rand() % 2 + 1;
   stringstream ss("");
   if(hit == 1){//got hit 
      double damage = damageCalc(dmg, def);
      hp -= damage;
      if (hp <= 0){
         ss << s.name() << "deals " << damage << "damage to " << name() << + " (0 HP).";
         cc->setLastAction(ss);
         onDeath(s);
         return 1;
      }
      ss << s.name() << "deals " << damage << "damage to " << name() << " (" << hp << " HP).";
      cc->setLastAction(ss);
      return 0;
   }else{//evaded
      ss << s.name() << "attacks halfling and missed";
      cc->setLastAction(ss);
      return 0;
   }
}

string Halfling::name(){
   return "Halfling";
}

char Halfling::display(){
   return 'L'; 
}
