#include <string>
#include "merchant.h"

using namespace std;

Merchant::Merchant(){
   hp = 30;
   atk = 70;
   def = 5;
}

int Merchant::generateGold(){
   return 4;
}

string Merchant::name(){
   return "Merchant";
}

char Merchant::display() {
   return 'M';
}
