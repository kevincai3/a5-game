#ifndef __SPRITE_H__
#define __SPRITE_H__
#include <string>
#include <iostream>

class Tile;
class ChamberCrawler;

class Sprite {
   protected:
      ChamberCrawler * cc;
      double hp, maxHp, atk, def, gold;
      Tile * pos;
      double damageCalc(double att, double def); //Performs damage calculation
      virtual void scanArea(); //Print out what the character sees around the 1 block radius.
   public:
      Sprite();
      virtual ~Sprite();

      void setChamberCrawler(ChamberCrawler * cc); //Sets cc class for Action disaply
      void setTile(Tile * pos);
      Tile * getTile();
      bool isDead();

      virtual std::string name() = 0; //Returns name of character.
      virtual void attack(Sprite &s); //Takes in oppenent as parameter
      virtual int defend(Sprite &s, double dmg); //returns 1 if kills, returns 0 if doesn't
      virtual void gainGold(char event, int amount); //G = gold pile, K = kill
      virtual void onKill(); //on Kill event
      virtual void move(Tile * newTile); // moving
      virtual void endTurn(); //For every turn effects.
      virtual void heal(double amount); //Also used to reduce hp.
      virtual void onDeath(Sprite &s) = 0; //For hero, tell main class gameover, for enemy, give gold to hero and die.
      virtual char display() = 0;
};

#endif
