#ifndef __GOBLIN_H__
#define __GOBLIN_H__
#include <string>
#include "hero.h"

class Goblin : public Hero {
   public:
      Goblin();
      std::string name();
      void onKill();
};
#endif
