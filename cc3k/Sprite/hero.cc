#include <sstream>
#include "hero.h"
#include "../chambercrawler.h"
#include "../tile.h"
#include "../Items/items.h"

using namespace std;

void Hero::scanArea(){
   for (int i = 0; i < 8; i++){
      if (pos->getNeighbour(i) != NULL && pos->getNeighbour(i)->display() == 'P'){ //If has potion
         string output = "unknown";
         for (unsigned int j = 0; j < usedPots.size(); j++){ //Checks if potio has been used before
            if (usedPots[j] == pos->getNeighbour(i)->getItem()->name()){
               output = usedPots[j];
            }
         }
         stringstream ss("");
         ss << name() << " sees a(n) " << output << " potion " << pos->mapDirection(i) << ".";
         cc->setLastAction(ss);
      }
   }
}

Hero::~Hero(){}

void Hero::move(Tile * newTile){
      pos->removeSprite();
      setTile(newTile);
      if (newTile->getItem()){ //Case for only treasure
         newTile->getItem()->useItem(*this);
      }
      newTile->setSprite(this);
      scanArea(); //Scans area for potions
}

void Hero::attack(Sprite &s){
   if (s.name() == "Merchant"){
      cc->setAggressiveMerchant(); //Flag for future merchant to attack
   }
   s.defend(*this, atk);
}

void Hero::gainGold(char event, int amount){
   stringstream ss("");
   gold += amount;
   if (event == 'K'){
      ss << name() << " has recieved " << amount << " gold.";
   } else {
      ss << name() << " has picked up " << amount << " gold.";
   }
   cc->setLastAction(ss);
}

void Hero::newFloor(){
   currentAtk = atk;
   currentDef = def;
}

void Hero::usePotion(string name, double hp, double atk, double def){
   stringstream ss("");
   heal(hp);
   currentAtk += atk;
   currentDef += def;
   ss << this->name() << " used " << name << ". ";
   cc->setLastAction(ss);
   usedPots.push_back(name); //Adds potion to repoitore
}

void Hero::onDeath(Sprite &s){ //Does nothing on purpose
}

double Hero::getScore(){
   return gold;
}

char Hero::display(){
   return '@';
}

ostream &operator<<(ostream &out, Hero &h){
   out << "Race: " << h.name() << " Gold: " << h.gold << endl;
   out << "HP: " << h.hp << endl;
   out << "Atk: " << h.currentAtk << endl;
   out << "Def: " << h.currentDef << endl;
   return out;
}
