#include <string>
#include "orcs.h"

using namespace std;

Orcs::Orcs(){
   hp = 180;
   atk = 30;
   def = 25;
}

string Orcs::name(){
   return "Orcs";
}

void Orcs::attack(Sprite &s){
   double dmg = atk;
   if (s.name() == "Goblin"){
      dmg *= 1.5;
   }
   s.defend(*this, dmg);
}

char Orcs::display(){
   return 'O';
}
