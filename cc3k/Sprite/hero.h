#ifndef __HERO_H__
#define __HERO_H__
#include <vector>
#include <string>
#include <iostream>
#include "sprite.h"

class Hero : public Sprite {
   protected:
      double currentAtk, currentDef;
      std::vector<std::string> usedPots; //Keeps track of used potions
      void scanArea(); //Print out what the character sees around the 1 block radius.
   public:
      virtual ~Hero();
      // p = gold pile, e = from enemy
      void newFloor(); //on floor change, reset current atk and def
      void move(Tile * newTile);
      char display();
      void onDeath(Sprite &s);
      virtual void gainGold(char event, int amount); // 'K' = kill, 'T' = treasure
      virtual void attack(Sprite &s);
      virtual void usePotion(std::string name, double hp = 0, double atk = 0, double def = 0); //Potions only apply their effects on the corresponding field
      virtual double getScore();
      friend std::ostream &operator<<(std::ostream &out, Hero &h); // For displaying hero status
};
#endif
