#ifndef __DROW_H__
#define __DROW_H__
#include <string>
#include "hero.h"

class Drow : public Hero {
   public:
      Drow();
      std::string name();
      void usePotion(std::string name, double hp = 0, double atk = 0, double def = 0);
};
#endif
