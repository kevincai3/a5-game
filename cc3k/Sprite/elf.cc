#include <string.h>
#include "elf.h"

using namespace std;

Elf::Elf(){
   hp = 140;
   atk = 30;
   def = 10;
}

string Elf::name(){
   return "Elf";
}

void Elf::attack(Sprite &s){
   int status = s.defend(*this, atk); //First strike
   if (status == 0){ //If hero lives, strike again
      s.defend(*this, atk);
   }
}

char Elf::display(){
   return 'E';
}
