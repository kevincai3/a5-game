#ifndef __MERCHANT_H__
#define __MERCHANT_H__
#include <string>
#include "enemy.h"

class Merchant : public Enemy {
   protected:
      int generateGold();
   public:
      Merchant();
      std::string name();
      char display();
};
#endif
