#ifndef __ORCS_H__
#define __ORCS_H__
#include <string>
#include "enemy.h"
#include "sprite.h"

class Orcs : public Enemy {
   public:
      Orcs();
      std::string name();
      void attack(Sprite &s);
      char display();
};

#endif
