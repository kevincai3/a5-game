#include <string>
#include "vampire.h"
#include "../chambercrawler.h"

using namespace std;

Vampire::Vampire(){
   hp = 50;
   maxHp = 99999;
   atk = currentAtk = 25;
   def = currentDef = 25;
}

string Vampire::name(){
   return "Vampire";
}

void Vampire::attack(Sprite &s){
   if (s.name() == "Merchant"){
      cc->setAggressiveMerchant();
   }
   if (s.name() == "Dwarf"){
      this->heal(-5);
   } else {
      this->heal(5);
   }
   s.defend(*this, currentAtk);
}
