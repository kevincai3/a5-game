#ifndef __ELF_H__
#define __ELF_H__
#include <string.h>
#include "enemy.h"
#include "sprite.h"

class Elf : public Enemy {
   public:
      Elf();
      std::string name();
      void attack(Sprite &s);
      char display();
};
#endif
