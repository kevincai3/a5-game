#include <cmath>
#include "sprite.h"
#include "sstream"

#include "../chambercrawler.h"
#include "../tile.h"

using namespace std;

void Sprite::scanArea(){
   //blank on purpose
}

Sprite::Sprite(){
   gold = 0;
}

Sprite::~Sprite(){}

void Sprite::setChamberCrawler(ChamberCrawler * cc){
   this->cc = cc;
}

double Sprite::damageCalc(double att, double def){
   return ceil((100/(100+def))*att);
}

void Sprite::attack(Sprite &s){
   s.defend(*this, atk);
}

int Sprite::defend(Sprite &s, double dmg){
   stringstream ss("");
   double damage = damageCalc(dmg, def);
   hp -= damage;
   if (floor(hp) <= 0){ //If slain
      ss << s.name() << " deals " << damage << " damage to " << this->name() << + " (0 HP).";
      cc->setLastAction(ss);
      onDeath(s);
      return 1;
   }
   //If not slain
   ss << s.name() << " deals " << damage << " damage to " << this->name() << " (" << hp << " HP).";
   cc->setLastAction(ss);
   return 0;
}

void Sprite::gainGold(char event, int amount){
   //Blank by design
}

void Sprite::onKill(){
   //Blank by design
}

void Sprite::move(Tile * newTile){
      pos->removeSprite();
      setTile(newTile);
      newTile->setSprite(this);
      scanArea();
}

void Sprite::endTurn(){
   //Blank by design
}
Tile * Sprite::getTile(){
return pos;
}
void Sprite::heal(double amount){
   hp += amount;
   if (floor(amount) != 0){
      stringstream ss("");
      ss << name() << " recieved " << amount << "HP.";
      cc->setLastAction(ss);
   }
}

void Sprite::setTile(Tile * pos){
   this->pos = pos;
}

bool Sprite::isDead(){
   return hp <= 0;
}
