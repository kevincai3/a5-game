#include <string>
#include "dragon.h"
#include "../chambercrawler.h"
#include "../tile.h"
#include "../Items/dhoard.h"

using namespace std;

Dragon::Dragon(DHoard *hoard):hoard(hoard){
   hp = 150;
   atk = def = 20;
}
 Dragon::~Dragon(){}
string Dragon::name(){
   return "Dragon";
}

void Dragon::onDeath(Sprite &s){
   hoard->setTakeable();
   stringstream ss("");
   ss << s.name() << " has slain " << name() << ".";
   cc->setLastAction(ss);
   s.onKill();
   pos->removeSprite();
}

char Dragon::display(){
   return 'D';
}
