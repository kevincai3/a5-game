#ifndef __DRAGON_H__
#define __DRAGON_H__
#include <string>
#include "enemy.h"

class DHoard;

class Dragon : public Enemy {
   DHoard *hoard;
   public:
      Dragon(DHoard * hoard);
      ~Dragon();
      std::string name();
      void onDeath(Sprite &s);
      char display();
};
#endif
