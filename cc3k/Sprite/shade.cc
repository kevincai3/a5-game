#include <string>
#include "shade.h"

using namespace std;

Shade::Shade(){
   hp = maxHp = 1250;
   //hp = maxhp = 125
   atk = currentAtk = 25;
   def = currentDef = 25;
}

string Shade::name(){
   return "Shade";
}

double Shade::getScore(){
   return gold * 1.5;
}
