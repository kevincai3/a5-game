#include <string>
#include "troll.h"

using namespace std;

Troll::Troll(){
   //hp = maxHp = 120;
   hp = maxHp = 1200;
   atk = currentAtk = 25;
   def = currentDef = 15;
}

string Troll::name(){
   return "Troll";
}

void Troll::endTurn(){
   double heal = 5; //Default behaviour
   if (hp >= maxHp) { //Hp exceeds maximum heal
      return;
   } else if (hp > maxHp - 5){ //Heal for less than 5
      heal = maxHp - hp;
   }
   this->heal(heal);
}
