#include <string>
#include <sstream>
#include <iostream>
#include "chambercrawler.h"
#include "Sprite/hero.h"
#include "tile.h"
#include "Items/items.h"

#include "Sprite/shade.h"
#include "Sprite/drow.h"
#include "Sprite/vampire.h"
#include "Sprite/goblin.h"
#include "Sprite/troll.h"

using namespace std;

ChamberCrawler::ChamberCrawler(): map(NULL), pc(NULL){
   init();
   dflt = true;
   lvl = new string[5];
}

ChamberCrawler::~ChamberCrawler(){
   if (pc) delete pc;
   if (map) delete map;
   if (lvl) delete [] lvl;
}
//check if merchants inthe game are aggressive to the hero
bool ChamberCrawler::aggressiveMerchant(){
   return merchantFlag;
}
//sets the aggressiveness of the merchant 
void ChamberCrawler::setAggressiveMerchant(){
   merchantFlag = true;
}
//sets game default flag true;
void ChamberCrawler::changeDflt(){
   dflt = false;
}
//Start new game, pass the race that user picked as arg
void ChamberCrawler::newGame(char race){
   if(!newgame){
      clearGame();
   }
   newgame= false;//game started so its not a new game anymore
   map = new Grid;
   map->setChamberCrawler(this);
   switch(race){
      case 's': pc= new Shade();//shade
                break;
      case 'd': pc= new Drow;//drow
                break;
      case 'v': pc= new Vampire();//vampire
                break;
      case 'g': pc= new Goblin();//goblin
                break;
      case 't': pc= new Troll();//troll
                break;
   }
   pc->setChamberCrawler(this);
   map->generateMap(lvl[level],dflt,pc);

}

//Generates a based on a string passed by user
void ChamberCrawler::setLevels(string map){

   //maps of every level will be based on theses strings
   lvl[0] = map;

   lvl[1] = map;

   lvl[2] = map;

   lvl[3] = map;

   lvl[4] = map;
}
//checks if the direction that play directs is valid
bool ChamberCrawler::validDir(string ss, string cmd){
   //mapping to direction set to no direction
   int mapping = -1;

   if (ss == "nw"){//north west
      mapping = 0;//setting mapping to the direction
   } else if (ss == "no"){//north
      mapping = 1;//....
   } else if (ss == "ne"){//north east...
      mapping = 2;
   } else if (ss == "we"){//...
      mapping = 3;
   } else if (ss == "ea"){
      mapping = 4;
   } else if (ss == "sw"){
      mapping = 5;
   } else if (ss == "so"){
      mapping = 6;
   } else if (ss == "se"){
      mapping = 7;
   } else {//case that user gives invalid input
      return false;
   }
   if (pc->getTile()->getNeighbour(mapping) && pc->getTile()->getNeighbour(mapping)->isEnterableHero() ||//checks directed tile is not NULL and is enterable by heros
         ((cmd == "a") && (pc->getTile()->getNeighbour(mapping) && pc->getTile()->getNeighbour(mapping)->getSprite())) || //checks if directed tile is not NULL and has a enemy for hero to attack
         ((cmd == "u") && (pc->getTile()->getNeighbour(mapping) && pc->getTile()->getNeighbour(mapping)->getItem()->heroSteppable() == false)//cchecks if not NULL and is stepable by heros
          && pc->getTile()->getNeighbour(mapping)->getItem()->name() != "DHoard")){//check that itemm is not a dragon hoard
      if(pc->getTile()->getNeighbour(mapping)->getStairs()){//if its is stairs
         dir = 0;//direction doesnt matter
         isNextFloor = true;//set flag
      }else{
         dir = mapping;//set mapping of direction
      }
   }
   else{
      return false;//not a valid directed tile
   }
   return true;//valid
}

//gets the command from user
void ChamberCrawler::takeCmd(string s){
   istringstream buffer(s);
   string ss;
   buffer >>ss;
   if(ss == "u"){//When PC uses a potion
      buffer>>ss;
      if(validDir(ss, "u") && pc->getTile()->getNeighbour(dir)->getItem()){//checks whether directed tile hold an item and is valid
         pc->getTile()->getNeighbour(dir)-> getItem()->useItem(*pc);//use the item
      }else{
         //invalid input-------------------
         cout<<"Error: Command not valid"<< endl;
      }
   }else if(ss == "a"){//When PC attacks
      buffer >>ss;
      if(validDir(ss,"a") && pc->getTile()->getNeighbour(dir)->getSprite()){//check whether directed tile holds a sprite and is valid
         pc->attack(*pc->getTile()->getNeighbour(dir)->getSprite());//attacks the enemy sprite
      }else{
         //invalid input-------------------

         cout<<"Error: Command not valid"<< endl;

      }
   }else if(validDir(ss,"")){//Play command to move
      stringstream ss("");
      ss << pc->name() << " moves " << s << ".";
      setLastAction(ss);
      pc->move(pc->getTile()->getNeighbour(dir));//moves to tile
      if (isNextFloor){//checks if player moved on to next floor
         if (level == 4){//check if play has won
            won = true;
            cout << "You Won" << endl;
            cout << "Your Score is: " << pc->getScore() << endl;//scores play accorddingly
         }
         else{
            level++;//increase the floor level
            map->wipeMap();	    
            map = new Grid;
            map->generateMap(lvl[level], dflt, pc);//generate the next floor
            stringstream ss("");
            ss << pc->name() << " advance to next level.";
            setLastAction(ss);
            print();
            clearLastAction();
            pc->newFloor();
         }
      }


   }else{

      //invalid input-------------------

      cout<<"Error: Command not valid"<< endl;

   }
   if(!isNextFloor){//if play still in same floor after move
      update();//check status and updates game
      map->enemyAction();//enemy AI is called
      update();;//check status and updates game
      pc->endTurn();//turn officially ends
      print();//outputs result
   }else{
      isNextFloor = false;//resets the flag
   }
}


//initializes the fields
void ChamberCrawler::init(){
   won = false;
   lost = false;
   merchantFlag = false;
   newgame = true;
   level = 0;
   prevAct.str("");
   isNextFloor = false;
   dir = -1;
}

//clears the current game
void ChamberCrawler::clearGame(){
   init();
   map->wipeMap();
   cout<<"progress";
   delete pc;
}

void ChamberCrawler::update(){
   if(pc->isDead()){
      lost =true;
   }
}
//displays the map and statistics
void ChamberCrawler::print(){
   cout << *map;
   cout << "Floor " << level+1 << endl;
   cout << *pc;
   cout << "Action: " << prevAct.str() << endl;
}
//sets the actions for output
void ChamberCrawler::setLastAction(stringstream &ss){
   prevAct << ss.str() << " ";
}
//clears the actions
void ChamberCrawler::clearLastAction(){
   prevAct.str("");
}

//returns whether its a new game 
bool ChamberCrawler::isNewGame(){
   return newgame;
}

bool ChamberCrawler::isLost(){
   return lost;
}

bool ChamberCrawler::isWon(){
   return won;
}
