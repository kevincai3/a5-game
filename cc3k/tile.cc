#include "tile.h"
#include "Items/items.h"

using namespace std;

Tile::Tile():pc(NULL), item(NULL){

}

Tile::~Tile(){
}
//gets the proper character to be displayed

void Tile::setStairs(){

}
bool Tile::getStairs(){
return false;
}

//see if tile is enterable
bool Tile::isEnterableHero(){
   bool enterable= false;
   if(heroIsTaken()){
   enterable = false;
   }
   return enterable;
}
//see if tile is enterable
bool Tile::isEnterableEnemy(){
  bool enterable= false;
     if(enemyIsTaken()){
   enterable = false;
   }
   return enterable;
}
//see whether or not hero can step on the tile
bool Tile::heroIsTaken(){

   if(pc == NULL && (item == NULL || item->heroSteppable())){
      return false;
   } else {
      return true;
   }
}

//see whether or not hero can step on the tile
bool Tile::enemyIsTaken(){
   if (pc == NULL && item == NULL){
      return false;
   } else {
      return true;
   }
}

//sets a sprite
void Tile::setSprite(Sprite * s){
   pc = s;
}

//gets a sprite
Sprite * Tile::getSprite(){
   return pc;
}

//removes the sprite
void Tile::removeSprite(){
   pc = NULL;
}
//sets a item
void Tile::setItem(Items *i){
   item = i;
}

Items * Tile::getItem(){
   return item;
}

//removes the item
void Tile::removeItem(){
   item = NULL;
}

string Tile::mapDirection(int i){
   string rtn;
   switch(i){
      case 0:
         rtn = "nw";
		 break;
      case 1:
		  rtn = "no";
		  break;
      case 2:
		  rtn = "ne";
		  break;
      case 3:
		  rtn = "we";
		  break;
      case 4:
		  rtn = "ea";
		  break;
      case 5:
		  rtn = "sw";
		  break;
      case 6:
		  rtn = "so";
		  break;
      case 7:
		  rtn = "se";
		  break;
   }
   return rtn;
}

void Tile::setNeighbour(Tile *t){
   neighbours.push_back(t);
}

int Tile::numOfNeighbours(){
   return neighbours.size();
}

Tile * Tile::getNeighbour(int i){
   return neighbours.at(i);
}
