#ifndef __DW_H__
#define __DW_H__
#include "tile.h"

class Doorway : public Tile{
   public:
      Doorway();
      ~Doorway();
      char display();
      bool isEnterableHero();
      bool isEnterableEnemy();
};
#endif
