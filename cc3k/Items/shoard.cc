#include "shoard.h"
#include "../Sprite/hero.h"
#include "../tile.h"

using namespace std;

string SHoard::name(){
   return "Small Hoard";
}

void SHoard::useItem(Hero &h){
   h.gainGold('T', 2);
   tile->removeItem();
}
