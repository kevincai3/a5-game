#ifndef __BDPOTION_H__
#define __BDPOTION_H__

#include "potion.h"

class BDPotion : public Potion{
   public:
      std::string name();
      void useItem(Hero &h);
};

#endif
