#include "phpotion.h"
#include "../Sprite/hero.h"
using namespace std;

string PHPotion::name(){
   return "PH";
}

void PHPotion::useItem(Hero &h){
   h.usePotion(name(), -10, 0, 0);
   tile->removeItem();
}
