#ifndef __NGOLD_H__
#define __NGOLD_H__
#include "treasure.h"

class Hero;

class NGold : public Treasure{
   public:
      std::string name();
      void useItem(Hero &h);
};
#endif
