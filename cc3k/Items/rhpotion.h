#ifndef __RHPOTION_H__
#define __RHPOTION_H__

#include "potion.h"

class RHPotion : public Potion{
   public:
      std::string name();
      void useItem(Hero &h);
};

#endif
