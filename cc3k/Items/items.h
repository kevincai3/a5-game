#ifndef __ITEMS_H__
#define __ITEMS_H__
#include <iostream>
#include <string>
#include "../Sprite/hero.h"
#include "../tile.h"

class Items {
   protected:
      Tile *tile;
   public:
      virtual ~Items();
      void setTile(Tile * tile);
      virtual bool heroSteppable();
      virtual void useItem(Hero &h) = 0;
      virtual std::string name() = 0;
      virtual char display();
};
#endif
