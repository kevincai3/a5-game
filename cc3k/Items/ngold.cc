#include "ngold.h"
#include "../Sprite/hero.h"
using namespace std;

string NGold::name(){
   return "Normal Goldpile";
}

void NGold::useItem(Hero &h){
   h.gainGold('T', 1);
   tile->removeItem();
}
