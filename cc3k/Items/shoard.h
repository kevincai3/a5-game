#ifndef __SHOARD_H__
#define __SHOARD_H__

#include "treasure.h"

class SHoard : public Treasure{
   public:
      std::string name();
      void useItem(Hero &h);
};
#endif
