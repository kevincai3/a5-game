#ifndef __DHOARD_H__
#define __DHOARD_H__

#include "treasure.h"

class Hero;

class DHoard : public Treasure{
   bool dragonSlain;
   public:
      DHoard();
      std::string name();
      void setTakeable();
      bool heroSteppable();
      void useItem(Hero &h);
};
#endif
