#ifndef __POTION_H__
#define __POTION_H__
#include "items.h"
#include <string>

class Hero;

class Potion : public Items{
   public:
      virtual ~Potion();
      char display();
      virtual std::string name() = 0;
      virtual void useItem(Hero &h) = 0;
};

#endif
