#include "rhpotion.h"
#include "../Sprite/hero.h"
using namespace std;

string RHPotion::name(){
   return "RH";
}

void RHPotion::useItem(Hero &h){
   h.usePotion(name(), 10, 0, 0);
   tile->removeItem();
}
