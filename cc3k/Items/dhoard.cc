#include "dhoard.h"
#include "../Sprite/hero.h"
#include "../tile.h"

using namespace std;

DHoard::DHoard(){
   dragonSlain = false;
}

string DHoard::name(){
   return "Dragon Hoard";
}

void DHoard::setTakeable(){
   dragonSlain = true;
}

void DHoard::useItem(Hero &h){
   h.gainGold('T', 6);
   tile->removeItem();
   delete this;
}

bool DHoard::heroSteppable(){
   return dragonSlain;
}
