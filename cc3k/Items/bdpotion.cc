#include "bdpotion.h"
#include "../Sprite/hero.h"
using namespace std;

string BDPotion::name(){
   return "BD";
}

void BDPotion::useItem(Hero &h){
   h.usePotion(name(), 0, 0, 5);
   tile->removeItem();
}
