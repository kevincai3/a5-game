#ifndef __WDPOTION_H__
#define __WDPOTION_H__

#include "potion.h"

class WDPotion : public Potion{
   public:
      std::string name();
      void useItem(Hero &h);
};

#endif
