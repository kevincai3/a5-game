#ifndef __WAPOTION_H__
#define __WAPOTION_H__

#include "potion.h"

class WAPotion : public Potion{
   public:
      std::string name();
      void useItem(Hero &h);
};

#endif
