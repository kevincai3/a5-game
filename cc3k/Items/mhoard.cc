#include "mhoard.h"
#include "../Sprite/hero.h"
#include "../tile.h"

using namespace std;

string MHoard::name(){
   return "Merchant Hoard";
}

void MHoard::useItem(Hero &h){
   h.gainGold('T', 4);
   tile->removeItem();
}
