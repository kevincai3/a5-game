#ifndef __BAPOTION_H__
#define __BAPOTION_H__

#include "potion.h"

class BAPotion : public Potion{
   public:
      std::string name();
      void useItem(Hero &h);
};

#endif
