#include "bapotion.h"
#include "../Sprite/hero.h"
using namespace std;

string BAPotion::name(){
   return "BA";
}

void BAPotion::useItem(Hero &h){
   h.usePotion(name(), 0, 5, 0);
   tile->removeItem();
}
