#ifndef __PHPOTION_H__
#define __PHPOTION_H__

#include "potion.h"

class PHPotion : public Potion{
   public:
      std::string name();
      void useItem(Hero &h);
};

#endif
