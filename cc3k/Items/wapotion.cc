#include "wapotion.h"
#include "../Sprite/hero.h"
using namespace std;

string WAPotion::name(){
   return "WA";
}

void WAPotion::useItem(Hero &h){
   h.usePotion(name(), 0, -5, 0);
   tile->removeItem();
}

