#include "wdpotion.h"
#include "../Sprite/hero.h"
using namespace std;

string WDPotion::name(){
   return "WD";
}

void WDPotion::useItem(Hero &h){
   h.usePotion(name(), 0, 0, -5);
   tile->removeItem();
}
