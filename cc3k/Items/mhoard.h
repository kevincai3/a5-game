#ifndef __MHOARD_H__
#define __MHOARD_H__

#include "treasure.h"

class Hero;

class MHoard : public Treasure{
   public:
      std::string name();
      void useItem(Hero &h);
};
#endif
