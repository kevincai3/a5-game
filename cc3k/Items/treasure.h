#ifndef __TREASURE_H__
#define __TREASURE_H__
#include <string>
#include <iostream>
#include "items.h"

class Hero;

class Treasure : public Items{
   public:
      virtual ~Treasure();
      char display();
      virtual bool heroSteppable();
      virtual std::string name() = 0;
      virtual void useItem(Hero &h) = 0;
};
#endif
