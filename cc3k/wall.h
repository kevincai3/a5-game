#ifndef __WALL_H__
#define __WALL_H__
#include "tile.h"

class Wall : public Tile{
   char symbol;
   public:
      Wall(char display);
      ~Wall();
      char display();
      bool isEnterableHero();
      bool isEnterableEnemy();
};
#endif
