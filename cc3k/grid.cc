#include <iostream>
#include <cmath>
#include <ctime>
#include "grid.h"

#include "doorway.h"
#include "floor.h"
#include "passage.h"
#include "wall.h"
#include "chambercrawler.h"

#include "Sprite/drow.h"
#include "Sprite/goblin.h"
#include "Sprite/sprite.h"
#include "Sprite/troll.h"
#include "Sprite/vampire.h"

#include "Sprite/dwarf.h"
#include "Sprite/elf.h"
#include "Sprite/human.h"
#include "Sprite/orcs.h"
#include "Sprite/vampire.h"
#include "Sprite/merchant.h"
#include "Sprite/halfling.h"
#include "Sprite/dragon.h"

#include "Items/rhpotion.h"
#include "Items/bapotion.h"
#include "Items/bdpotion.h"
#include "Items/phpotion.h"
#include "Items/wapotion.h"
#include "Items/wdpotion.h"

#include "Items/ngold.h"
#include "Items/shoard.h"
#include "Items/mhoard.h"
#include "Items/dhoard.h"
using namespace std;

Grid::Grid(){
   for (int i = 0; i < GRIDX; i++){
      for (int j = 0; j < GRIDY; j++){
         grid[i][j] = NULL;
         mapCpy[i][j] = '\0';
      }
   }
   numOChambers = 0;
}

Grid::~Grid(){
	wipeMap();
}
//clears the map
void Grid::wipeMap(){

   int size;
   size =listOChambers.size();
   for (int i = 0; i < size; i++){
      listOChambers[i].clear();
      listOChambers[i].resize(0);
   }
   for (int i = 0; i < GRIDX; i++){
      for (int j = 0; j < GRIDY; j++){
         if (grid[i][j]) delete grid[i][j];
         grid[i][j] = NULL;
         mapCpy[i][j] = '\0';
      }
   }
   for (int i = 0; i < elist.size(); i++){
      delete elist[i];
   }
   for (int i = 0; i < plist.size(); i++){
      delete plist[i];
   }
   for (int i = 0; i < glist.size(); i++){
      delete glist[i];
   }
   listOChambers.clear();
   elist.clear();
   plist.clear(); 
   glist.clear(); 
   listOChambers.resize(0);
   elist.resize(0);
   plist.resize(0);
   glist.resize(0);

   
   for (int i = 0; i < GRIDX; i++){
      for (int j = 0; j < GRIDY; j++){

        if (grid[i][j]) {delete grid[i][j];}
         mapCpy[i][j] = '\0';
      }
   }

}
//look through neighbours of tiles to find a hero used for combat process
Sprite * Grid::attackHero(Tile * t){
   for (int i = 0; i < 8; i++){
      if (t->getNeighbour(i) && t->getNeighbour(i)->display() == '@'){
         return t->getNeighbour(i)->getSprite();
      }
   }
   return NULL;
}
//checks the neighbours of tile for enterable non taken tiles
bool Grid::freeNeighbours(Tile * t){
   for(int i =0;i<t->numOfNeighbours();i++){
      if (t->getNeighbour(i)->isEnterableEnemy()){
         return true;
      }
   }
   return false;
}
//checks if enemy can move to the neighbour of tile t in the direction of dir
bool Grid::moveEnemy(Tile * t,  int dir){
   if (t->getNeighbour(dir) && t->getNeighbour(dir)->isEnterableEnemy()){
      t->getSprite()->move(t->getNeighbour(dir));
      return true;
   }
   return false;
}

void Grid::setChamberCrawler(ChamberCrawler * cc){
   this->cc = cc;
}
//enemy ai actions
void Grid::enemyAction(){
   copyMap();//copy of the map to loop through

   for(int i = 0; i < GRIDX; i++){
      for(int j = 0; j < GRIDY; j++){
      //look to see if hero is in a neighbour of grid[i][j]


         Sprite * Hero = attackHero(grid[i][j]);
         if((mapCpy[i][j] == 'H' || mapCpy[i][j] == 'W' || mapCpy[i][j] == 'E' ||
                  mapCpy[i][j] == 'O'|| mapCpy[i][j] == 'L' || mapCpy[i][j] == 'D') && Hero ){//if hero is neighbour of a enemy they fight
            grid[i][j]->getSprite()->attack(*Hero);//fight each other
         } else if (mapCpy[i][j] == 'M' && cc->aggressiveMerchant() && Hero){//if merchant is hostile to hero and they are neighbours
            grid[i][j]->getSprite()->attack(*Hero);//fight each other
         }
         else if(mapCpy[i][j] == 'H' || mapCpy[i][j] == 'W' || mapCpy[i][j] == 'E' ||
               mapCpy[i][j] == 'O' || mapCpy[i][j] == 'M' || mapCpy[i][j] == 'L'){//if tile contains a enemy 
            if(freeNeighbours(grid[i][j])){//has a neighbour tile that they can move onto
               bool success = false;
               while(!success){
                  int dir = rand() % 8;
                  success = moveEnemy(grid[i][j], dir);//randomly move onto the free tile thru trial and error
               }
            }
         }
      }
   }
}
//builds list of list of tiles in chambers
void Grid::buildChamberList(int x, int y){
   if (x < 0 || x > GRIDX || y < 0 || y > GRIDY){
      return;
   }
   else if(mapCpy[x][y] == '.'){
      mapCpy[x][y] = ' ';
      listOChambers[listOChambers.size()-1].push_back(grid[x][y]);
      buildChamberList(x-1, y-1);
      buildChamberList(x-1, y);
      buildChamberList(x-1, y+1);
      buildChamberList(x, y-1);
      buildChamberList(x, y+1);
      buildChamberList(x+1, y-1);
      buildChamberList(x+1, y);
      buildChamberList(x+1, y+1);
   }
}
//makes a char copy of the map
void Grid::copyMap(){
   for(int i =0;i<GRIDX;i++){
      for(int j =0;j<GRIDY;j++){
         mapCpy[i][j]=grid[i][j]->display();
      }
   }
}
//counts the number of chambers in the map
void Grid::countNumOChambers(){
   copyMap();
   for(int i =0;i<GRIDX;i++){
      for(int j =0;j<GRIDY;j++){
         if(mapCpy[i][j] == '.'){
            numOChambers++;
            listOChambers.resize(numOChambers);
            buildChamberList(i, j);
         }
      }
   }
}
//spawns enemies using random numbers
void Grid::spawnEnemies(){
   int type;
   int dragon = elist.size();

   for(int i = dragon;i<MAXNUMOFENEMIES;i++){
      srand (time(NULL));
      type = rand() % 18 +1;
      if(type<5){
         elist.push_back(new Human);
      }else if(type<6){
         elist.push_back(new Elf);
      }else if(type<7){
         elist.push_back(new Orcs);
      }else if(type<8){
         elist.push_back(new Merchant);
      }else if(type<11){
         elist.push_back(new Dwarf);
      }else{
         elist.push_back(new Halfling);
      }
   }
   int chamber;
   int itile;
   bool success = false;
   for(int i =dragon;i<MAXNUMOFENEMIES;i++){
      chamber = rand() % numOChambers;
      while(!success){
         itile = rand() % listOChambers.at(chamber).size();
         if(listOChambers.at(chamber).at(itile)->isEnterableEnemy()){
            success = true;
            listOChambers.at(chamber).at(itile)->setSprite(elist.at(i));
            elist.at(i)->setTile(listOChambers.at(chamber).at(itile));
            elist.at(i)->setChamberCrawler(cc);
         }
      }
      success = false;
   }

}
//spawns potions
void Grid::spawnPotion(){
   int type;
   for(int i =0;i<MAXNUMOFPOTIONSNGOLD;i++){
      srand (time(NULL));
      type = rand() % 6 +1;
      switch(type){
         case 1: plist.push_back(new RHPotion);
                 break;
         case 2: plist.push_back(new BAPotion);
                 break;
         case 3: plist.push_back(new BDPotion);
                 break;
         case 4: plist.push_back(new PHPotion);
                 break;
         case 5: plist.push_back(new WAPotion);
                 break;
         case 6: plist.push_back(new WDPotion);
                 break;
      }
   }
   int chamber;
   int itile;
   bool success = false;
   for (int i = 0; i<MAXNUMOFPOTIONSNGOLD; i++){
      chamber = rand() % numOChambers;
      while(!success){
         itile = rand() % listOChambers.at(chamber).size();
         if(listOChambers.at(chamber).at(itile)->isEnterableEnemy()){
            success = true;
            listOChambers.at(chamber).at(itile)->setItem(plist.at(i));
            plist.at(i)->setTile(listOChambers.at(chamber).at(itile));
         }
      }
      success = false;
   }

}
//spawns gold
void Grid::spawnGold(){
   int type;
   for(int i =0;i<MAXNUMOFPOTIONSNGOLD;i++){
      srand (time(NULL));
      type = rand() % 8 + 1;
      if(type<3){
         glist.push_back(new SHoard);
      }else if(type==8){
         glist.push_back(new DHoard);
         elist.push_back(new Dragon(dynamic_cast<DHoard*>(glist.at(glist.size()-1))));
      }else{
         glist.push_back(new NGold);
      }
   }
   int chamber;
   int itile;
   bool success = false;
   for(int i =0;i<MAXNUMOFPOTIONSNGOLD;i++){
      chamber = rand() % numOChambers;
      while(!success){
         itile = rand() % listOChambers.at(chamber).size();
         if(glist.at(i)->name()=="Dragon Hoard"){
            if(listOChambers.at(chamber).at(itile)->isEnterableEnemy()){
               if(freeNeighbours(listOChambers.at(chamber).at(itile))){
                  listOChambers.at(chamber).at(itile)->setItem(glist.at(i));
                  glist.at(i)->setTile(listOChambers.at(chamber).at(itile));
                  while(!success){
                     int tNeighbour = rand() % 8; 
                     if(listOChambers.at(chamber).at(itile)->getNeighbour(tNeighbour)->isEnterableEnemy()){
                        success = true;
                        listOChambers.at(chamber).at(itile)->getNeighbour(tNeighbour)->setSprite(elist.at(elist.size()-1));
                        elist.at(elist.size()-1)->setChamberCrawler(cc);
                        elist.at(elist.size()-1)->setTile(listOChambers.at(chamber).at(itile)->getNeighbour(tNeighbour));
                     }
                  } 
               }
            }
         }else{
            if(!listOChambers.at(chamber).at(itile)->enemyIsTaken()){
               success = true;
               listOChambers.at(chamber).at(itile)->setItem(glist.at(i));
               glist.at(i)->setTile(listOChambers.at(chamber).at(itile));
            }
         }
      }
      success = false;
   }

}

//spawns the hero
void Grid::spawnHero(Hero * pc){
   int chamber;
   int itile;
   bool success = false;
   while(!success){
      srand (time(NULL));
      chamber = rand() % numOChambers;
      itile = rand() % listOChambers.at(chamber).size();
      if(listOChambers.at(chamber).at(itile)->isEnterableEnemy()){
         success = true;
         listOChambers.at(chamber).at(itile)->setSprite(pc);
         pc->setTile(listOChambers.at(chamber).at(itile));
      }else{
         cout<<"spawned failed"<< numOChambers<<endl;
      }
   }
   spawnStair(abs(chamber-1));
}
//spawns the stairs
void Grid::spawnStair(int chamber){
   int itile;
   bool success = false;
   while(!success){
      srand (time(NULL));
      itile = rand() % listOChambers.at(chamber).size();
      if(listOChambers.at(chamber).at(itile)->isEnterableEnemy()){
         success = true;
         listOChambers.at(chamber).at(itile)->setStairs();
      }else{
         cout<<"spawned failed"<< numOChambers<<endl;
      }
   }
}
//generates a map  using the string representation of the map
//if default map it will spawn creatures and items randomly 
void Grid::generateMap(string lvl, bool dflt, Hero* pc){

   char * tiles = new char[lvl.size()+1]; //char array of the whole map
   tiles[lvl.size()]= '\0';
   memcpy(tiles, lvl.c_str(), lvl.size());
   int index = 0;
   for(int i = 0; i < GRIDX; i++){
      for(int j = 0; j < GRIDY; j++){
         if (i == 3 && j == GRIDX){
         }
         switch(tiles[index]){
            case '|': grid[i][j] = new Wall('|');
                      break;
            case '-': grid[i][j] = new Wall('-');
                      break;
            case ' ': grid[i][j] = new Wall(' ');
                      break;
            case '.': grid[i][j] = new Floor;
                      break;
            case '+': grid[i][j] = new Doorway;
                      break;
            case '#': grid[i][j] = new Passage;
                      break;
            case '\\': grid[i][j] = new Floor;
                       grid[i][j]->setStairs();
                       break;
            case '0': grid[i][j] = new Floor;
                      grid[i][j]->setItem(new RHPotion);
                      grid[i][j]->getItem()->setTile(grid[i][j]);
                      break;
            case '1':grid[i][j] = new Floor;
                     grid[i][j]->setItem(new BAPotion);
                     grid[i][j]->getItem()->setTile(grid[i][j]);
                     break;
            case '2':grid[i][j] = new Floor;
                     grid[i][j]->setItem(new BDPotion);
                     grid[i][j]->getItem()->setTile(grid[i][j]);
                     break;
            case '3':grid[i][j] = new Floor;
                     grid[i][j]->setItem(new PHPotion);
                     grid[i][j]->getItem()->setTile(grid[i][j]);
                     break;
            case '4':grid[i][j] = new Floor;
                     grid[i][j]->setItem(new WAPotion);
                     grid[i][j]->getItem()->setTile(grid[i][j]);
                     break;
            case '5':grid[i][j] = new Floor;
                     grid[i][j]->setItem(new WDPotion);
                     grid[i][j]->getItem()->setTile(grid[i][j]);
                     break;
            case '6':grid[i][j] = new Floor;
                     grid[i][j]->setItem(new NGold);
                     grid[i][j]->getItem()->setTile(grid[i][j]);
                     break;
            case '7':grid[i][j] = new Floor;
                     grid[i][j]->setItem(new SHoard);
                     grid[i][j]->getItem()->setTile(grid[i][j]);
                     break;
            case '8':grid[i][j] = new Floor;
                     grid[i][j]->setItem(new MHoard);
                     grid[i][j]->getItem()->setTile(grid[i][j]);
                     break;
            case '9':grid[i][j] = new Floor;
                     grid[i][j]->setItem(new DHoard);
                     grid[i][j]->getItem()->setTile(grid[i][j]);
                     break;
            case '@':grid[i][j] = new Floor;
                     grid[i][j]->setSprite(pc);
                     pc->setTile(grid[i][j]);
                     break;
            case 'H':grid[i][j] = new Floor;
                     elist.push_back(new Human);
                     grid[i][j]->setSprite(elist.at(elist.size()-1));
                     elist.at(elist.size()-1)->setTile(grid[i][j]);
                     elist.at(elist.size() - 1)->setChamberCrawler(cc);
                     break;
            case 'W':grid[i][j] = new Floor;
                     elist.push_back(new Dwarf);
                     grid[i][j]->setSprite(elist.at(elist.size()-1));
                     elist.at(elist.size()-1)->setTile(grid[i][j]);
                     elist.at(elist.size() - 1)->setChamberCrawler(cc);
                     break;
            case 'E':grid[i][j] = new Floor;
                     elist.push_back(new Elf);
                     grid[i][j]->setSprite(elist.at(elist.size()-1));
                     elist.at(elist.size()-1)->setTile(grid[i][j]);
                     elist.at(elist.size() - 1)->setChamberCrawler(cc);
                     break;
            case 'O':grid[i][j] = new Floor;
                     elist.push_back(new Orcs);
                     grid[i][j]->setSprite(elist.at(elist.size()-1));
                     elist.at(elist.size()-1)->setTile(grid[i][j]);
                     elist.at(elist.size() - 1)->setChamberCrawler(cc);
                     break;
            case 'M':grid[i][j] = new Floor;
                     elist.push_back(new Merchant);
                     grid[i][j]->setSprite(elist.at(elist.size()-1));
                     elist.at(elist.size()-1)->setTile(grid[i][j]);
                     elist.at(elist.size() - 1)->setChamberCrawler(cc);
                     break;
            case 'D':grid[i][j] = new Floor;
                     elist.push_back(new Dragon(NULL));
                     grid[i][j]->setSprite(elist.at(elist.size()-1));
                     elist.at(elist.size()-1)->setTile(grid[i][j]);
                     elist.at(elist.size() - 1)->setChamberCrawler(cc);
                     break;
            case 'L':grid[i][j] = new Floor;
                     elist.push_back(new Halfling);
                     grid[i][j]->setSprite(elist.at(elist.size()-1));
                     elist.at(elist.size()-1)->setTile(grid[i][j]);
                     elist.at(elist.size() - 1)->setChamberCrawler(cc);
                     break;
         }
         index++;
      }
   }
//setting neighbours
   for(int i = 0; i < GRIDX; i++){
      for(int j = 0; j < GRIDY; j++){
         if(((i-1) >= 0)&&((j-1) >= 0)){
            grid[i][j]->setNeighbour(grid[i-1][j-1]);
         }else{
            grid[i][j]->setNeighbour(NULL);
         }
         if(((i-1)>=0)){
            grid[i][j]->setNeighbour(grid[i-1][j]);
         }else{
            grid[i][j]->setNeighbour(NULL);
         } 

         if(((i-1)>=0)&&((j+1)<GRIDY)){
            grid[i][j]->setNeighbour(grid[i-1][j+1]);
         }else{
            grid[i][j]->setNeighbour(NULL);
         } 

         if(((j-1)>=0)){
            grid[i][j]->setNeighbour(grid[i][j-1]);
         }else {
            grid[i][j]->setNeighbour(NULL);
         }

         if(((j+1)<GRIDY)){
            grid[i][j]->setNeighbour(grid[i][j+1]);
         }else{
            grid[i][j]->setNeighbour(NULL);
         }

         if(((i+1)<GRIDX)&&((j-1)>=0)){
            grid[i][j]->setNeighbour(grid[i+1][j-1]);
         }else {
            grid[i][j]->setNeighbour(NULL);
         }

         if(((i+1)<GRIDX)){
            grid[i][j]->setNeighbour(grid[i+1][j]);
         }else {
            grid[i][j]->setNeighbour(NULL);
         }

         if(((i+1)<GRIDX)&&((j+1)<GRIDY)){
            grid[i][j]->setNeighbour(grid[i+1][j+1]);
         }else{
            grid[i][j]->setNeighbour(NULL);
         }
      }
   }
//spawning items enemies heroes if its default map
   if(dflt){
      countNumOChambers();
      spawnHero(pc);
      spawnPotion();
      spawnGold();
      spawnEnemies();

   }
   delete [] tiles;
}



ostream &operator<<(ostream &out, Grid &g){
   for (int i = 0; i < GRIDX; i++){
      for (int j = 0; j < GRIDY; j++){
         out << g.grid[i][j]->display();
      }
      out << endl;
   }
   return out;
}
