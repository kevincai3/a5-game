#include "floor.h"
#include "Sprite/sprite.h"
#include "Items/items.h"
Floor::Floor(){
   hasStairs = false;
}
Floor::~Floor(){}
char Floor::display(){
   if (pc){
      return pc->display();
   } else if (item){
      return item->display();
   } else if (getStairs()){
   return '\\';
   }else{

      return '.';
   }
}
void Floor::setStairs(){
   hasStairs = true; 
}
bool Floor::getStairs(){
   return hasStairs;
}
//see if tile is enterable
bool Floor::isEnterableHero(){
   bool enterable= true;
   if(heroIsTaken()){
      enterable = false;
   }

   return enterable;
}
//see if tile is enterable
bool Floor::isEnterableEnemy(){
   bool enterable= true;
   if(enemyIsTaken()){
      enterable = false;
   }
   if(hasStairs){
      return false;
   }
   return enterable;
}
