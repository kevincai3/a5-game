#include <iostream>
#include <fstream>
#include <stdexcept> 
#include "chambercrawler.h"
using namespace std;

int main(int argc, char** argv){

   string in;
   char race;
   bool gLoop = true;
   //game class object
   ChamberCrawler *cc = new ChamberCrawler; 
   string map = "";//string version of the map

   if(argc>1){//check if there are arguments in the command line
      try{
         ifstream file(argv[argc-1]);//read the file specified in the command line
         if(file.is_open()){
            while(getline(file,in)){//read the file and concatenate all the data into a string
               map = map+ in;
            }
         }else{
            cout << "Error: Unable to open file"<< endl;
         }
         cc->changeDflt();//switch program to non default inplementation
      }catch(out_of_range &e){//catch any errors that may occur 
         //due to too many arguments in command line
         cout<<"Error: too many arguments at the command line"<< endl;
      }
   }
   if (map == ""){ //If file cannot be read or if no file provided
      ifstream file("default.txt");//use the default map, by reading it from this file
      if(file.is_open()){
         while(getline(file,in)){
            map = map+ in;
         }
      }else{
         cout << "Unable to open file"<< endl;
      }
   }

   //game loop
   while(gLoop){

      //intro to the game
      cout << endl;

      cout<<"ChamberCrawler3000"<<endl;
      cout << endl;
      cout<<"new game"<<endl;

      cout << endl;
      cout<<"pick a race: (s)hade, (d)row, (v)vampire, (g)oblin, (t)roll or default Shade"<<endl;
      cout << endl;
      while(getline(cin, in)){
         if(cc->isNewGame()){//if its a new game
            cc->setLevels(map);
            if(in == "s"){ race='s';}
            else if(in=="d"){race='d';}
            else if(in== "v") {race='v';}
            else if(in== "g") {race='g';}
            else if(in== "t") {race='t';}
            else{race='s';}//default character shade
            cc->newGame(race);//creates a new game with hero specified by race
            cc->print();//display output

            cout<<"Commands:"<<endl;
            cout<<"Move To: (no)rth, (ea)st, (we)st, (so)uth, (nw)est, (ne)ast, (sw)est, (se)ast"<<endl;
            cout<<"Use Item At: (u)se <Direction> ; Example: u no -> use potion at the north"<< endl;
            cout<<"Attack At: (a)ttack <Direction> ; Example: a so -> attack enemy at the south"<<endl;
            cout<<"(r)estart, (q)uit"<<endl;
            cout<<"Enter Command: "<<endl;

         }else{
            if(in== "r"){//if player wishes to restart the game
               cc->clearGame();
               break;
            }//player wants to quit the game
            else if(in=="q"){
               gLoop = false;//switch game loop flag
               break;
            }
            else if(in=="p"){//if player wants to play again
               if (cc->isLost()){
                  cc->clearGame();
                  cc->newGame(race);
                  cc->print();

                  cout<<"Commands:"<<endl;
                  cout<<"Move To: (no)rth, (ea)st, (we)st, (so)uth, (nw)est, (ne)ast, (sw)est, (se)ast"<<endl;
                  cout<<"Use Item At: (u)se <Direction> ; Example: u no -> use potion at the north"<< endl;
                  cout<<"Attack At: (a)ttack <Direction> ; Example: a so -> attack enemy at the south"<<endl;
                  cout<<"(r)estart, (q)uit"<<endl;
                  cout<<"Enter Command: "<<endl;

               }
               else{ //invalid input give by user results in lost of a turn
                  cout << "Error: Invalid command" << endl;
               }
            }else{
               if(!cc->isLost()){//takes command from player during game		
                  cc->takeCmd(in);
               }
               if(cc->isLost()){//if game is lost
                  //Print out defeat msg
                  cout<<"You Died: (p)lay again, (r)estart, (q)uit"<<endl;                        
               }else if(cc->isWon()){//if game is won
                  cc->clearGame();//clear game
                  break;
               }else{//if game is happening
                  cc->clearLastAction(); //Clear last action field
                  cout<<"Commands:"<<endl;

                  cout<<"Move To: (no)rth, (ea)st, (we)st, (so)uth, (nw)est, (ne)ast, (sw)est, (se)ast"<<endl;
                  cout<<"Use Item At: (u)se <Direction> ; Example: u no -> use potion at the north"<< endl;
                  cout<<"Attack At: (a)ttack <Direction> ; Example: a so -> attack enemy at the south"<<endl;
                  cout<<"(r)estart, (q)uit"<<endl;                
                  cout<<"Enter Command: "<<endl;
               }
            }
         }
      }
   }
   delete cc;
}
