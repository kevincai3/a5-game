#ifndef __GRID_H__
#define __GRID_H__
#include <iostream>
#include <vector>
#include <cstdlib>
//foward declarations
class Tile;
class Enemy;
class Potion;
class Treasure;
class Hero;
class Sprite;
class ChamberCrawler;
//constant values
const int MAXNUMOFENEMIES = 20;
const int MAXNUMOFPOTIONSNGOLD = 10;
const int GRIDX = 25;
const int GRIDY = 79;
class Grid{
   Tile *grid[GRIDX][GRIDY];
   char mapCpy[GRIDX][GRIDY];
   ChamberCrawler * cc;
   int numOChambers;
   std::vector<std::vector<Tile*> > listOChambers;
   std::vector<Enemy*> elist;
   std::vector<Potion*> plist;
   std::vector<Treasure*> glist;

  
   Sprite * attackHero(Tile * t); //Returns null if hero is not around, otherwise returns the hero is on.
   bool moveEnemy(Tile * t, int dir); //returns true if enemy can move and did move in that direction, otherwise returns false.
   public:
      Grid();
      ~Grid();
       void wipeMap();
      void copyMap();
      void setChamberCrawler(ChamberCrawler * cc);
      void enemyAction();
      void buildChamberList(int x, int y);
      void countNumOChambers();
      
      void spawnStair(int chamber);
      void spawnEnemies();
      void spawnPotion();
      bool freeNeighbours(Tile * t);
      void spawnGold();
      void spawnHero(Hero * pc);
      void generateMap(std::string lvl, bool dflt, Hero * pc);

      friend std::ostream &operator<<(std::ostream &out, Grid &g);
};
#endif
